﻿using GameStore.Domain.Entities;
using System.Data.Entity;

namespace GameStore.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        //создание класса контекста, который будет ассоциировать модель с базой данных.
    }
}