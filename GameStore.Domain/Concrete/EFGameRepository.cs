﻿using System.Collections.Generic;
using GameStore.Domain.Entities;
using GameStore.Domain.Abstract;

namespace GameStore.Domain.Concrete
{
    public class EFGameRepository : IGameRepository
    {
        EFDbContext context = new EFDbContext();
        //экземпляр EFDbContext для извлечения данных из базы 

        public IEnumerable<Game> Games
        {
            get { return context.Games; }
        }
    }
}
